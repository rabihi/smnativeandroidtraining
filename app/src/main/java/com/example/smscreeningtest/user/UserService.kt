package com.example.smscreeningtest.user

import retrofit2.Call
import retrofit2.http.GET

interface UserService {
    @GET("api/users")
    fun getUsersAll(): Call<UserResponse>
}