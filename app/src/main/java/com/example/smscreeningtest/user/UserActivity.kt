package com.example.smscreeningtest.user

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smscreeningtest.databinding.ActivityUserBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class UserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserBinding
    lateinit var progerssProgressDialog: ProgressDialog
    private var listItems = ArrayList<Data>()
    lateinit var adapter:DataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //setting up the adapter
        binding.recyclerView.adapter = DataAdapter(listItems, this)
        binding.recyclerView.layoutManager= LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        progerssProgressDialog=ProgressDialog(this)
        progerssProgressDialog.setTitle("Loading")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()

        getCurrentData()
    }

    private fun getCurrentData() {
        val call: Call<UserResponse> = ApiClient.getClient.getUsersAll()
        call.enqueue(object : Callback<UserResponse> {

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                progerssProgressDialog.dismiss()
                listItems.addAll(response.body()!!.data)
                binding.recyclerView.adapter?.notifyDataSetChanged()
                Log.d("SALAH", listItems.joinToString(separator = ":"))
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable?) {
                progerssProgressDialog.dismiss()
                Log.d("SALAH", t.toString())
            }

        })
    }

}