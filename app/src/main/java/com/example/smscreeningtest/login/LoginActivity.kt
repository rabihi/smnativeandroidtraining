package com.example.smscreeningtest.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.smscreeningtest.databinding.ActivityLoginBinding
import com.example.smscreeningtest.exoplayer.ExoplayerAtivity
import com.example.smscreeningtest.home.HomeActivity
import com.example.smscreeningtest.model.LoginModel

//import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val fieldPalindrome = binding.fieldPalindromeText.text
        val fieldName = binding.fieldNamaText.text
        val presenter = LoginPresenter(this)

        binding.buttonCheckPalindrome.setOnClickListener{
            presenter.checkPalindrome(fieldPalindrome.toString())

        }

        binding.buttonNext.setOnClickListener{
            if(fieldName.toString()==""){
                val builder = AlertDialog.Builder(this)
                builder.setMessage("Please fill name field")
                builder.setPositiveButton("Ok"){dialogInterface, which ->
                }
                val alertDialog = builder.create()
                alertDialog.show()
            }else{
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra("userName", fieldName.toString());
                startActivity(intent)
            }

        }

        binding.buttonExo.setOnClickListener{
            val intent = Intent(this, ExoplayerAtivity::class.java)
            startActivity(intent)
        }

    }

    override fun checkPalindromeResult(data: LoginModel) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(data.isPalindrome)
        builder.setPositiveButton("Ok"){dialogInterface, which ->
        }
        val alertDialog = builder.create()
        alertDialog.show()
    }
}