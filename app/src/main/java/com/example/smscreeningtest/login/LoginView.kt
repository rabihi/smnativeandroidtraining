package com.example.smscreeningtest.login

import com.example.smscreeningtest.model.LoginModel

interface LoginView {

    fun checkPalindromeResult(data: LoginModel)

}