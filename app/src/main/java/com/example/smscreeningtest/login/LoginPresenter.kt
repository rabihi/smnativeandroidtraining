package com.example.smscreeningtest.login
import android.util.Log
import android.widget.Toast
import com.example.smscreeningtest.model.LoginModel

class LoginPresenter(var data: LoginView) {

    fun checkPalindrome(palindrome : String){
        var isPalindrome : Boolean? = null
        var result : String? = null

        isPalindrome = palindrome.equals(palindrome.reversed(), ignoreCase = true)

        if(isPalindrome){
            result = "is Palindrome"
        }else{
            result = "is not palindrome"
        }

        val model = LoginModel()


        model.isPalindrome = result
        data.checkPalindromeResult(model)
    }

}